import qi

import time
import sys
import base64
import requests
import json
import cv2
import numpy as np
import threading

labels = {
    'aeroplane': [128, 0, 0],
    'background': [0, 0, 0],
    'bag': [128, 64, 128],
    'bed': [0, 191, 128],
    'bedclothes': [0, 64, 191],
    'bench': [128, 191, 128],
    'bicycle': [0, 128, 0],
    'bird': [128, 128, 0],
    'boat': [0, 0, 128],
    'book': [64, 64, 0],
    'bottle': [128, 0, 128],
    'building': [191, 64, 0],
    'bus': [0, 128, 128],
    'cabinet': [64, 191, 0],
    'car': [128, 128, 128],
    'cat': [64, 0, 0],
    'ceiling': [191, 191, 0],
    'chair': [191, 0, 0],
    'cloth': [64, 64, 128],
    'computer': [191, 64, 128],
    'cow': [64, 128, 0],
    'cup': [64, 191, 128],
    'curtain': [64, 128, 64],
    'dog': [64, 0, 128],
    'door': [191, 191, 128],
    'fence': [0, 0, 64],
    'floor': [128, 0, 64],
    'flower': [0, 128, 64],
    'food': [128, 128, 64],
    'grass': [0, 0, 191],
    'ground': [128, 0, 191],
    'horse': [191, 0, 128],
    'keyboard': [0, 128, 191],
    'light': [128, 128, 191],
    'motorbike': [64, 128, 128],
    'mountain': [64, 0, 64],
    'mouse': [191, 0, 64],
    'person': [191, 128, 128],
    'plate': [191, 0, 191],
    'platform': [191, 128, 64],
    'pottedplant': [0, 64, 0],
    'road': [64, 128, 191],
    'rock': [191, 128, 191],
    'sheep': [128, 64, 0],
    'shelves': [0, 64, 64],
    'sidewalk': [128, 64, 64],
    'sign': [64, 0, 191],
    'sky': [0, 191, 64],
    'snow': [128, 191, 64],
    'sofa': [0, 191, 0],
    'table': [191, 128, 0],
    'track': [128, 64, 191],
    'train': [128, 191, 0],
    'tree': [0, 191, 191],
    'truck': [128, 191, 191],
    'tvmonitor': [0, 64, 128],
    'wall': [64, 64, 64],
    'water': [191, 64, 64],
    'window': [64, 191, 64],
    'wood': [191, 191, 64]
}

class SemanticLabeller:
    def __init__(self, session):
        session.waitForService('ImageStream')

        self.stream = session.service('ImageStream')
        self.last_mask = None
        self.mutex = threading.Lock()

    def getMask(self):
        '''
        Gets a semantic image mask based on the front facing camera stream
        '''
        encoded = self.stream.getImage(0, False)
        encoded = encoded[22:]

        encoded_mask = self._requestMask(encoded)

        mask = base64.b64decode(encoded_mask)
        mask = np.fromstring(mask, np.uint8)
        mask = cv2.imdecode(mask, cv2.CV_LOAD_IMAGE_COLOR)

        self.last_mask = mask

        return 'data:image/jpg;base64,' + str(encoded_mask)

    def getMaskOverlaid(self):
        '''
        Gets an image from the front facing camera overlayed with semantic labels
        '''
        encoded = self.stream.getImage(0, False)
        encoded = encoded[22:]

        mask = self._requestMask(encoded)

        img = base64.b64decode(encoded)
        mask = base64.b64decode(mask)
        mask = np.fromstring(mask, np.uint8)
        img = np.fromstring(img, np.uint8)

        foreground = cv2.imdecode(mask, cv2.CV_LOAD_IMAGE_COLOR)
        background = cv2.imdecode(img, cv2.CV_LOAD_IMAGE_COLOR)

        self.last_mask = foreground

        output = foreground.copy()

        cv2.addWeighted(background, 0.95, foreground, 1, 0.0, output);
        result = cv2.imencode('.jpg', output)

        return 'data:image/jpg;base64,' + str(base64.b64encode(result[1]))

    def getLabelAtXY(self, x, y):
        if self.last_mask == None:
            return None

        try:
            label = self._getLabel(self.last_mask[y][x])
            return label

        except Exception as e:
            print str(e)

        return 'background'

    def _requestMask(self, encoded):
        resp = requests.post('http://cloudvis.qut.edu.au/ws/refinenet', headers={'content-type': 'application/json'}, data=json.dumps({'image': encoded}))
        return resp.json()['image']

    def _getLabel(self, pixel):
        lowest_error = float('inf')
        best_match = 'background'

        for key in labels:
            diff_r = abs(pixel[0] - labels[key][2])
            diff_g = abs(pixel[1] - labels[key][1])
            diff_b = abs(pixel[2] - labels[key][0])

            error = diff_r + diff_g + diff_b

            if error < lowest_error:
                lowest_error = error
                best_match = key

        return best_match

def main():
    app = qi.Application()
    app.start()
    session = app.session

    semantic_labeller = SemanticLabeller(session)
    session.registerService('SemanticLabeller', semantic_labeller)
    app.run()



if __name__ == '__main__':
    main()
