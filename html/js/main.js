/* globals $, QiSession */

var clickEventType = ((document.ontouchstart !== null) ? 'click' : 'touchstart')

var labeller = false
var tts = false

var phrases = [
  'that looks like a ',
  'I think that\'s a ',
  'I can see a ',
  'I know that\'s a ',
  'I recognize a ',
  'that might be a ',
  'I\'m sure that\'s a '
]

$(document).ready(function () {
  QiSession(function (session) {
    connected(session)
  }, disconnected)

  $('#image').on(clickEventType, function (e) {
    if (e.type === 'touchstart') {
      e.pageX = e.originalEvent.touches[0].pageX
      e.pageY = e.originalEvent.touches[0].pageY
    }

    var width = 320
    var height = 240

    var offset = $(this).offset()

    var xnorm = (e.pageX - offset.left) / e.target.width
    var ynorm = (e.pageY - offset.top) / e.target.height

    var rx = Math.floor(width * xnorm)
    var ry = Math.floor(height * ynorm)

    labeller.getLabelAtXY(rx, ry).then(function (label) {
      if (label === 'background') {
        return
      }

      var phrase = phrases[Math.floor(Math.random() * phrases.length)]
      tts.say(phrase + label)
    })
  })
})

function connected (session) {
  console.log('Connected')

  session.service('ALTextToSpeech').then(function (service) {
    tts = service
  })

  session.service('SemanticLabeller').then(function (service) {
    labeller = service
    update()
  })
}

function disconnected (session) {
  console.log('Disconnected')
}

function update () {
  if (!labeller) {
    return
  }
  labeller.getMaskOverlaid().then(function (data) {
    $('#image').attr('src', data)
    update()
  })
}
