<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Semantic Labelling" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="styles" src="html/css/styles.css" />
        <File name="index" src="html/index.html" />
        <File name="jquery-3.2.1.min" src="html/js/jquery-3.2.1.min.js" />
        <File name="main" src="html/js/main.js" />
        <File name="settings_manager" src="behavior_1/scripts/settings_manager.py" />
        <File name="semantic_labeller" src="scripts/semantic_labeller.py" />
        <File name="README" src="README.md" />
        <File name="Cube" src="html/Cube.png" />
        <File name="icon" src="icon.png" />
        <File name="Thumbs" src="html/Thumbs.db" />
        <File name="qiproject" src="qiproject.xml" />
        <File name="HexIcon" src="HexIcon.png" />
        <File name="Thumbs" src="Thumbs.db" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
    <qipython name="semantic_labeller" />
</Package>
